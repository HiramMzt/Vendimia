﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ExaOrdinario.CapaPresentacion
{
    public partial class FrmConfiguracion : ExaOrdinario.CapaPresentacion.FrmBase
    {
        private CapaDatos.BD obj = new CapaDatos.BD(Program.cadenaConexion);
        private CapaDatos.BD obj1 = new CapaDatos.BD(Program.cadenaConexion);
        public FrmConfiguracion()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            
            int idModificar;
            obj.Consulta("Select isnull(max(id_configuracion), 0) From configuracion");
            if (obj.Dr.Read())
            {
                
                idModificar = int.Parse(obj.Dr.GetValue(0).ToString());
                if (idModificar > 0)
                {
                    //hay registros actualilzar
                    obj1.Consulta(String.Format("Update configuracion Set tasa ={0}, porcentaje_enganche = {1}, plazo_maximo = {2}, activo=1 Where id_configuracion={3}", txtTasa.Text, txtPorcentaje.Text, txtPlazo.Text, idModificar));                    
                    MessageBox.Show("Bien Hecho. La configuración ha sido modificada");
                }
                else {
                    //no hay registro insertar
                    obj1.Consulta(String.Format("Insert into configuracion(tasa, porcentaje_enganche, plazo_maximo, activo)  Values({0},{1},{2},{3})", txtTasa.Text, txtPorcentaje.Text, txtPlazo.Text, 1));
                    MessageBox.Show("Bien Hecho. La configuración ha sido registrada");
                }

            }

          
        }
    }
}
