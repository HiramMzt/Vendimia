﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ExaOrdinario.CapaPresentacion
{
    public partial class FrmArticulos : ExaOrdinario.CapaPresentacion.FrmBase
    {
        private CapaDatos.BD objBd = new CapaDatos.BD(Program.cadenaConexion);
        private CapaNegocios.LogicaNegocio objLog = new CapaNegocios.LogicaNegocio();
        public FrmArticulos()
        {
            InitializeComponent();
        }

        private void FrmArticulos_Load(object sender, EventArgs e)
        {

        }

        private void btnSiAceptar_Click(object sender, EventArgs e)
        {
            int continua = 1;
            string mensaje;
            if (txtDescripcion.Text == string.Empty) {
                continua = 0;
               MessageBox.Show("No es posible continuar, debe de ingresar Descripcion es obligatorio");
            }
            if (txtModelo.Text == string.Empty)
            {
                continua = 0;
                MessageBox.Show("No es posible continuar, debe de ingresar Modelo es obligatorio");
            }
            if (txtPrecio.Text == string.Empty)
            {
                continua = 0;
                MessageBox.Show("No es posible continuar, debe de ingresar Precio es obligatorio");
            }
            else if(objLog.esFlotante(txtPrecio.Text)==false){
                continua = 0;
                MessageBox.Show("Ingrese un precio válido");
                txtPrecio.Focus();
            }
            if (txtExistencia.Text == string.Empty){
                continua = 0;
                MessageBox.Show("No es posible continuar, debe de ingresar Precio es obligatorio");
            }
            else if (objLog.esNumero(txtExistencia.Text) == false) {
                continua = 0;
                MessageBox.Show("Ingrese una cantidad válida");
                txtExistencia.Focus();
            }
            

            if(continua == 1){
                //Ahora a insertar el producto el base de datos
                this.objBd.Consulta(String.Format("Insert Into articulos(descripcion, modelo, precio, existencia, activo) VALUES('{0}','{1}',{2},{3},1)", txtDescripcion.Text, txtModelo.Text, txtPrecio.Text, txtExistencia.Text));
                MessageBox.Show("Artículo ingresado correctamente");
                txtDescripcion.Text = string.Empty;
                txtModelo.Text = string.Empty;
                txtPrecio.Text = string.Empty;
                txtExistencia.Text = string.Empty;
            }

        }
    }
}
