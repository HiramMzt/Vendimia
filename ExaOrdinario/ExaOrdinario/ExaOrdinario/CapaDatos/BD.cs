﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
namespace ExaOrdinario.CapaDatos
{

    using Microsoft.VisualBasic;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Data.SqlClient;
    public class BD
    {
        private SqlDataReader m_dr;
        private DataTable m_dt;
        private string m_stringConexion;
        private SqlConnection m_cnn;
        private SqlCommand cmd;

         public  BD(string cadenaConexion)
        {
            m_stringConexion = cadenaConexion;
            m_cnn = new SqlConnection(m_stringConexion);
        }
 
        public void Consulta(string query)
        {
            ArrayList al = new ArrayList();
            if (Cnn.State == System.Data.ConnectionState.Open)
            {
                cmd = new SqlCommand(query, m_cnn);
                Dr.Close();
                Dr = cmd.ExecuteReader();
            }
            else
            {
                m_cnn.Open();
                cmd = new SqlCommand(query, m_cnn);
                Dr = cmd.ExecuteReader();
            }
        }

        public void llenarCombo(String id, String campo, String tabla, ComboBox combo)
        {
            string consulta = String.Format("Select {0} as id, {1} as nombre From {2} ", id,campo,tabla);
            //DataSet ds = new DataSet();
            using (SqlConnection sc = new SqlConnection())
            {
                sc.ConnectionString = Program.cadenaConexion;
                sc.Open();
                using (SqlDataAdapter sda = new SqlDataAdapter(consulta, sc))
                {
                    DataTable dt = new DataTable();

                    sda.Fill(dt);
                    combo.ValueMember = "id";
                    combo.DisplayMember = "nombre";
                    combo.DataSource = dt;
                }
            }
        }

        public DataTable LlenarGrid(String query)
        {
            DataTable dt = new DataTable();
            if (Cnn.State == System.Data.ConnectionState.Open)
            {
                cmd = new SqlCommand(query, m_cnn);
                Dr.Close();
                Dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                //grid.DataSource = dt;
            }
            return dt;
        }

        public DataTable Dt
        {
            get { return m_dt; }
            set { m_dt = value; }
        }
      
 
        public SqlDataReader Dr
        {
            get { return m_dr; }
            set { m_dr = value; }
        }
 
        public void Cerrar()
        {
            Dr.Close();
            m_cnn.Close();
        }
 
        public SqlConnection Cnn
        {
            get { return m_cnn; }
            set { m_cnn = value; }
        }


       


    }
}
