﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using CapaDatos;
namespace ExaOrdinario.CapaNegocios
{
    //Esta clase maneja la logica del negociogocio, como pueden ser or olos calculos
    //las vaidacion de ventas, etc
    public class LogicaNegocio
    {
        //string strConexion="server=WIN-MZT-SIS-06\SQLEXPRESS; user id=sa; password=Dsdsistemas2012; database=dsd_mazatlan;";
        //Data Source=ADRIANA;Initial Catalog=master;Integrated Security=True
        string srCadena = "server=" + Properties.Settings.Default.servidor + "; user id=" + Properties.Settings.Default.usuario + "; password=" + Properties.Settings.Default.pasword + "; database=" + Properties.Settings.Default.baseDatos + ";";
        CapaDatos.BD conector = new CapaDatos.BD(Program.cadenaConexion);
        public LogicaNegocio()
        {
        }


        public void llenarGridClientes(System.Windows.Forms.DataGridView tabla)
        {
            tabla.Rows.Clear();
            conector.Consulta("Select id_cliente as clave, CONCAT(nombre, ' ', apellido_paterno, ' ',apellido_materno) AS nombre   From clientes WHere activo = 1");
            //DataGridViewRow row = (DataGridViewRow)dgvClientes.Rows
            while (conector.Dr.Read())
            {
                tabla.Rows.Add(conector.Dr.GetValue(0), conector.Dr.GetValue(1),"Editar");
            }
        }

        public int existencias(int idArticulo) {
            conector.Consulta(String.Format("Select existencia From articulos Where id_articulo = {0}" , idArticulo));
            if (conector.Dr.Read())
                return conector.Dr.GetInt32(0);
            else
                return 1;
        }

        public double precio(double precio){
            conector.Consulta(String.Format("Select tasa, plazo_maximo From configuracion Where activo = 1"));
            int plazo=12;
            float tasa;
            if (conector.Dr.Read())
            {
                System.Windows.Forms.MessageBox.Show(String.Format("La tasa es de {0} y el plazo es de {1}", conector.Dr.GetValue(0), conector.Dr.GetValue(1)));
                
                plazo = Int16.Parse(conector.Dr.GetValue(1).ToString());
                tasa = float.Parse(conector.Dr.GetValue(0).ToString());
                precio = precio * (1 + (tasa * plazo / 100));
                return Math.Round(precio, 2);
            }
            else
                return precio;
                    
        }
        public double enganche(double importe){
            float factor;
            conector.Consulta("Select porcentaje_enganche From configuracion Where activo = 1");
            if (conector.Dr.Read()) {
                factor = float.Parse(conector.Dr.GetValue(0).ToString());
                return Math.Round((factor / 100) * importe,2);
            }
            else {
                return Math.Round(importe,2);
            }

        }
        public double bonificacionEnganche(double enganche){
            double tasa;
            int plazo;
            conector.Consulta("Select tasa, plazo_maximo From configuracion Where activo = 1");
            if (conector.Dr.Read()) {
                tasa = double.Parse(conector.Dr.GetValue(0).ToString());
                plazo = int.Parse(conector.Dr.GetValue(1).ToString());
                return Math.Round(enganche * ((tasa * plazo) / 100),2);
            }
            return Math.Round(enganche,2);
        }
        public double totalAdeudo(double V_importe, double V_engache, double V_bonificacion){
            double resultado;
            resultado = Math.Round(V_importe - V_engache - V_bonificacion,2);
            return Math.Round(resultado,2);
        }

        public double[] generarAbonos(int meses, double total) {
            //obetner el precio de contado         
            conector.Consulta(String.Format("Select tasa, plazo_maximo From configuracion Where activo = 1"));
            int plazo = 12;
            if (conector.Dr.Read())
            {
                float tasa;
                double precioContado;
                double pagos;
                double totalPagos;
                double ahorro;
                double[] regresa;
                plazo = Int16.Parse(conector.Dr.GetValue(1).ToString());
                tasa = float.Parse(conector.Dr.GetValue(0).ToString());
                precioContado = total / (1 + ((tasa * plazo) / 100));
                totalPagos = precioContado * (1 + (tasa * meses) / 100);
                pagos = totalPagos / meses;
                ahorro = total - totalPagos;
                //System.Windows.Forms.MessageBox.Show(String.Format("A {0} meses son pagos a: {1} de un total de {2} y una ahorro de: {3}", meses, Math.Round(pagos,2), Math.Round(totalPagos, 2),Math.Round(ahorro,2)));
                double[] valores = { pagos, totalPagos, ahorro };

                return valores;
            }
            else
                return null;
            
        }

        public double precioContado(){
             return 22;
        }
        public double totalPagar(){
            return 29.86;
        }
        public double importeAbono(){
            return 0.78;
        }
        public double importeAhorro(){
           return 14.59;
        }

        public bool esNumero(string captura){
            try {
                int.Parse(captura);
                return true;
            }
            catch{
                return false;
            }
        }

        public bool esFlotante(string captura) {
            try
            {
                float.Parse(captura);
                return true;
            }
            catch {
                return false;
            }
        }

    }
}