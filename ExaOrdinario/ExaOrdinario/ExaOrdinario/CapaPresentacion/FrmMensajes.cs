﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExaOrdinario
{
    public partial class FrmMensajes : Form
    {
        public FrmMensajes(int tipo, string mensaje)
        {
            InitializeComponent();
            switch (tipo) { 
                    //tipo 1 SI/NO tipo 2 Aceptar cancelar
                case 1:
                    btnSiAceptar.Text = "SI";
                    btnNoCancelar.Text = "NO";
                    break;
                case 2:
                    btnSiAceptar.Text = "ACEPTAR";
                    btnNoCancelar.Text = "CANCELAR";
                    break;
                 
            }
            lblMensaje.Text = mensaje;
        }

        private void btnSiAceptar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNoCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
