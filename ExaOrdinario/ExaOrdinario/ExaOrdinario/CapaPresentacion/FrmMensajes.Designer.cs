﻿namespace ExaOrdinario
{
    partial class FrmMensajes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnNoCancelar = new System.Windows.Forms.Button();
            this.btnSiAceptar = new System.Windows.Forms.Button();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnNoCancelar);
            this.panel1.Controls.Add(this.btnSiAceptar);
            this.panel1.Location = new System.Drawing.Point(10, 59);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(420, 43);
            this.panel1.TabIndex = 0;
            // 
            // btnNoCancelar
            // 
            this.btnNoCancelar.Location = new System.Drawing.Point(239, 3);
            this.btnNoCancelar.Name = "btnNoCancelar";
            this.btnNoCancelar.Size = new System.Drawing.Size(127, 37);
            this.btnNoCancelar.TabIndex = 1;
            this.btnNoCancelar.Text = "NO";
            this.btnNoCancelar.UseVisualStyleBackColor = true;
            this.btnNoCancelar.Click += new System.EventHandler(this.btnNoCancelar_Click);
            // 
            // btnSiAceptar
            // 
            this.btnSiAceptar.Location = new System.Drawing.Point(54, 3);
            this.btnSiAceptar.Name = "btnSiAceptar";
            this.btnSiAceptar.Size = new System.Drawing.Size(127, 37);
            this.btnSiAceptar.TabIndex = 0;
            this.btnSiAceptar.Text = "SI";
            this.btnSiAceptar.UseVisualStyleBackColor = true;
            this.btnSiAceptar.Click += new System.EventHandler(this.btnSiAceptar_Click);
            // 
            // lblMensaje
            // 
            this.lblMensaje.AutoSize = true;
            this.lblMensaje.Location = new System.Drawing.Point(12, 9);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(35, 13);
            this.lblMensaje.TabIndex = 1;
            this.lblMensaje.Text = "label1";
            // 
            // FrmMensajes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 105);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "FrmMensajes";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnNoCancelar;
        private System.Windows.Forms.Button btnSiAceptar;
        private System.Windows.Forms.Label lblMensaje;
    }
}