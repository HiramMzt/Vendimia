﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ExaOrdinario.CapaPresentacion
{
    public partial class FrmHistoricoClientes : ExaOrdinario.CapaPresentacion.FrmBase
    {
        private CapaDatos.BD objBd = new CapaDatos.BD(Program.cadenaConexion);
        private CapaNegocios.LogicaNegocio objLog = new CapaNegocios.LogicaNegocio();
        FrmClientes pantalla = new FrmClientes();
        public FrmHistoricoClientes()
        {
            InitializeComponent();
        }

        private void btnNuevaVenta_Click(object sender, EventArgs e)
        {
          
            pantalla.frmPadre = this;
            pantalla.Show();

        }

         internal void llenarTabla()
        {
            objLog.llenarGridClientes(dgvVentas);

            /*dgvVentas.Rows.Clear();
            objBd.Consulta("Select id_cliente as clave, CONCAT(nombre, ' ', apellido_paterno, ' ',apellido_materno) AS nombre   From clientes WHere activo = 1");
            //DataGridViewRow row = (DataGridViewRow)dgvClientes.Rows
            while (objBd.Dr.Read())
            {
                dgvVentas.Rows.Add(objBd.Dr.GetValue(0), objBd.Dr.GetValue(1));
            }*/
        }

        public void FrmHistoricoClientes_Load(object sender, EventArgs e)
        {
            objBd.Consulta("Select id_cliente as clave, CONCAT(nombre, ' ', apellido_paterno, ' ',apellido_materno) AS nombre   From clientes WHere activo = 1");
            //DataGridViewRow row = (DataGridViewRow)dgvClientes.Rows
            while (objBd.Dr.Read())
            {
                dgvVentas.Rows.Add(objBd.Dr.GetValue(0), objBd.Dr.GetValue(1));
            }
        }

        private void dgvVentas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2) {
                pantalla.traerCliente(int.Parse(dgvVentas.CurrentRow.Cells[0].Value.ToString()));
                pantalla.Show();
            }
        }
    }
}
