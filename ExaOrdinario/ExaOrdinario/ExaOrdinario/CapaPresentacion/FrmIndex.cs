﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ExaOrdinario.CapaPresentacion
{
    public partial class FrmIndex : ExaOrdinario.CapaPresentacion.FrmBase
    {
        public FrmIndex()
        {
            InitializeComponent();
        }

        private void ventasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmVentasHistorial pantalla = new FrmVentasHistorial();
            pantalla.MdiParent = this;
            pantalla.Show();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmHistoricoClientes pantalla = new FrmHistoricoClientes();
            pantalla.MdiParent = this;
            pantalla.Show();
        }

        private void articulosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmArticulos pantalla = new FrmArticulos();
            pantalla.MdiParent = this;
            pantalla.Show();
        }

        private void configuracionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConfiguracion pantalla = new FrmConfiguracion();
            pantalla.MdiParent = this;
            pantalla.Show();
        }
    }
}
