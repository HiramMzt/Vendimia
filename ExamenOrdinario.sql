USE [ordinario]
GO
/****** Object:  Table [dbo].[articulos]    Script Date: 26/01/2017 04:03:11 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[articulos](
	[id_articulo] [int] NULL,
	[descripcion] [varchar](70) NULL,
	[modelo] [varchar](50) NULL,
	[precio] [float] NULL,
	[existencia] [int] NULL,
	[activo] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[clientes]    Script Date: 26/01/2017 04:03:11 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[clientes](
	[id_cliente] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](30) NULL,
	[apellido_paterno] [varchar](50) NULL,
	[apeliido_materno] [varchar](50) NULL,
	[rfc] [varchar](18) NULL,
	[activo] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[configuracion]    Script Date: 26/01/2017 04:03:11 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[configuracion](
	[id_configuracion] [int] IDENTITY(1,1) NOT NULL,
	[porcentaje_enganche] [float] NULL,
	[plazo_maximo] [int] NULL,
	[activo] [bit] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ventas]    Script Date: 26/01/2017 04:03:11 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ventas](
	[id_venta] [int] IDENTITY(1,1) NOT NULL,
	[folio] [varchar](50) NULL,
	[fecha] [date] NULL,
	[id_cliente] [int] NULL,
	[enganche] [float] NULL,
	[bonificacion_enganche] [float] NULL,
	[tot] [float] NULL,
	[activo] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ventas_detalle]    Script Date: 26/01/2017 04:03:11 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ventas_detalle](
	[id_venta_detalle] [int] IDENTITY(1,1) NOT NULL,
	[id_venta] [int] NULL,
	[id_articulo] [int] NULL,
	[cantidad] [int] NULL,
	[precio] [float] NULL,
	[importe] [float] NULL,
	[activo] [bit] NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[articulos] ADD  CONSTRAINT [DF_articulos_activo]  DEFAULT ((1)) FOR [activo]
GO
ALTER TABLE [dbo].[clientes] ADD  CONSTRAINT [DF_clientes_activo]  DEFAULT ((0)) FOR [activo]
GO
ALTER TABLE [dbo].[configuracion] ADD  CONSTRAINT [DF_configuracion_activo]  DEFAULT ((0)) FOR [activo]
GO
ALTER TABLE [dbo].[ventas] ADD  CONSTRAINT [DF_ventas_activo]  DEFAULT ((0)) FOR [activo]
GO
ALTER TABLE [dbo].[ventas_detalle] ADD  CONSTRAINT [DF_ventas_detalle_activo]  DEFAULT ((0)) FOR [activo]
GO
