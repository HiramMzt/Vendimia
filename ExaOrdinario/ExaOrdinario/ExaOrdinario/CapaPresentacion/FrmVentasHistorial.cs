﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ExaOrdinario.CapaPresentacion
{
    public partial class FrmVentasHistorial : ExaOrdinario.CapaPresentacion.FrmBase
    {
        private CapaDatos.BD objBd = new CapaDatos.BD(Program.cadenaConexion);
        public FrmVentasHistorial()
        {
            InitializeComponent();
        }

        private void FrmVentasHistorial_Load(object sender, EventArgs e)
        {
            objBd.Consulta("SELECT  v.id_venta, v.folio, c.id_cliente AS clave_cliente, Concat(c.nombre,' ',c.apellido_paterno,' ', c.apellido_materno) AS Nombre, v.tot, v.fecha, 'ACTIVA' AS estatus FROM ventas AS v INNER JOIN clientes AS c ON v.id_cliente = c.id_cliente WHERE v.activo = 1");
          
            //DataGridViewRow row = (DataGridViewRow)dgvClientes.Rows
            while (objBd.Dr.Read())
            {
                dgvVentas.Rows.Add(objBd.Dr.GetValue(0), objBd.Dr.GetValue(1), objBd.Dr.GetValue(2), objBd.Dr.GetValue(3), objBd.Dr.GetValue(4), String.Format("{0:d/MM/yy}", objBd.Dr.GetValue(5)), objBd.Dr.GetValue(6));
            }
        }

        private void btnNuevaVenta_Click(object sender, EventArgs e)
        {
            FrmVentas pantalla = new FrmVentas();
            pantalla.Show();
        }

        /*
         SELECT  v.id_venta, v.folio, c.id_cliente AS clave_cliente, c.nombre AS Nombre, v.tot, v.fecha, 'ACTIVA' AS estatus FROM ventas AS v INNER JOIN clientes AS c ON v.id_cliente = c.id_cliente WHERE v.activo = 1
         */

    }
}
