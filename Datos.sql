USE [ordinario]
GO
SET IDENTITY_INSERT [dbo].[articulos] ON 

INSERT [dbo].[articulos] ([id_articulo], [descripcion], [modelo], [precio], [existencia], [activo]) VALUES (1, N'COMEDOR 4 SILLAS', N'CARLOS V', 4250, 10, 1)
INSERT [dbo].[articulos] ([id_articulo], [descripcion], [modelo], [precio], [existencia], [activo]) VALUES (2, N'SILLON TERCIOPELO', N'EMPERADOR', 8500, 12, 1)
SET IDENTITY_INSERT [dbo].[articulos] OFF
SET IDENTITY_INSERT [dbo].[clientes] ON 

INSERT [dbo].[clientes] ([id_cliente], [nombre], [apellido_paterno], [apellido_materno], [rfc], [activo]) VALUES (1, N'Hiram', N'hernandez', N'garcia', N'1263544', 0)
INSERT [dbo].[clientes] ([id_cliente], [nombre], [apellido_paterno], [apellido_materno], [rfc], [activo]) VALUES (2, N'RODOLFO', N'TAPIA', N'GARCIA', N'000', 1)
INSERT [dbo].[clientes] ([id_cliente], [nombre], [apellido_paterno], [apellido_materno], [rfc], [activo]) VALUES (3, N'Gerardo Cambio', N'e', N'Garcia', N'ed', 1)
INSERT [dbo].[clientes] ([id_cliente], [nombre], [apellido_paterno], [apellido_materno], [rfc], [activo]) VALUES (4, N'ee', N'eded', N'ed', N'ede', 1)
INSERT [dbo].[clientes] ([id_cliente], [nombre], [apellido_paterno], [apellido_materno], [rfc], [activo]) VALUES (5, N'DED', N'EE', N'EDED', N'EE', 1)
INSERT [dbo].[clientes] ([id_cliente], [nombre], [apellido_paterno], [apellido_materno], [rfc], [activo]) VALUES (6, N'marisol', N'cortes', N'robles', N'ee', 1)
INSERT [dbo].[clientes] ([id_cliente], [nombre], [apellido_paterno], [apellido_materno], [rfc], [activo]) VALUES (7, N'dee', N'eded', N'22', N'w22w', 1)
INSERT [dbo].[clientes] ([id_cliente], [nombre], [apellido_paterno], [apellido_materno], [rfc], [activo]) VALUES (8, N'Hiram ', N'Hernandez', N'Garcia', N'hegh880305sl7', 1)
SET IDENTITY_INSERT [dbo].[clientes] OFF
SET IDENTITY_INSERT [dbo].[configuracion] ON 

INSERT [dbo].[configuracion] ([id_configuracion], [tasa], [porcentaje_enganche], [plazo_maximo], [activo]) VALUES (4, 2.8, 20, 12, 1)
SET IDENTITY_INSERT [dbo].[configuracion] OFF
