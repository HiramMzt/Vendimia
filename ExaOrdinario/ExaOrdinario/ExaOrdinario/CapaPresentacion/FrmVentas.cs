﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ExaOrdinario.CapaPresentacion
{
    public partial class FrmVentas : ExaOrdinario.CapaPresentacion.FrmBase
    {
        private CapaDatos.BD objBd = new CapaDatos.BD(Program.cadenaConexion);
        private CapaNegocios.LogicaNegocio objLog = new CapaNegocios.LogicaNegocio();
        private int folioActual = 0;
        private string rfc;
        private double precio;
        private float importe;
        private int PosSeleccio = -1;
        public FrmVentas()
        {
            InitializeComponent();
            objBd.Consulta("Select isnull(max(id_venta),0)+1 From ventas");
            if (objBd.Dr.Read())
                folioActual = objBd.Dr.GetInt32(0);
            else
                folioActual = 1;
            lblFolio.Text += String.Format(" {0:0.0}", folioActual);

        }

        private void FrmVentas_Load(object sender, EventArgs e)
        {
            objBd.llenarCombo("id_cliente", "Concat(nombre,' ',apellido_paterno,' ', apellido_materno) ", "clientes", cmbClientes);
            objBd.llenarCombo("id_articulo", "descripcion", "articulos", cmbArticulos);
        }

        private void cmbClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            string mensaje = cmbClientes.SelectedValue.ToString();
            objBd.Consulta(String.Format("Select rfc From clientes Where id_cliente = {0}",cmbClientes.SelectedValue));
            if (objBd.Dr.Read())
                rfc = objBd.Dr.GetString(0);
            else
                rfc = "000";
            lblRfc.Text = String.Format("RFC: {0}", rfc);
        }

        private void btnAgregarArt_Click(object sender, EventArgs e)
        {
            string modelo;
           
            objBd.Consulta(String.Format("Select modelo From articulos Where id_articulo = {0}", cmbArticulos.SelectedValue));
            if (objBd.Dr.Read())
                modelo = objBd.Dr.GetString(0);
            else
                modelo = "000";

             objBd.Consulta(String.Format("Select precio From articulos Where id_articulo = {0}", cmbArticulos.SelectedValue));
            if (objBd.Dr.Read())
                precio = objBd.Dr.GetDouble(0);

            //dgvVentas.Rows.Add(objBd.Dr.GetValue(0), objBd.Dr.GetValue(1), objBd.Dr.GetValue(2), objBd.Dr.GetValue(3), objBd.Dr.GetValue(4), String.Format("{0:d/MM/yy}", objBd.Dr.GetValue(5)), objBd.Dr.GetValue(6));
            dgvDetalle.Rows.Add(cmbArticulos.SelectedValue,cmbArticulos.Text, modelo, 0, String.Format("{0:00}",objLog.precio(precio).ToString()), 0, "x");
        }

        private void dgvDetalle_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            /*FrmMensajes pantalla = new FrmMensajes(1, "HOLA");
            pantalla.ShowDialog();*/
        }

        private void dgvDetalle_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvDetalle.CurrentCell.ColumnIndex == 3)
            {
                
            }
        }

        private void dgvDetalle_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            //int validaIndice = 0;
            //validaIndice = int.Parse(dgvDetalle.CurrentRow.Cells[0].Value.ToString());
            //if(validaIndice > 0){
            try
            {
                int.Parse(dgvDetalle.CurrentRow.Cells[0].Value.ToString());
                if (e.ColumnIndex == 3)
                {
                    if (!this.objLog.esNumero(e.FormattedValue.ToString()))
                    {
                        MessageBox.Show("El dato introducido no una cantidad Valida", "Error de validación",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                        dgvDetalle.Rows[e.RowIndex].ErrorText = "El dato introducido no es cantidad";
                        e.Cancel = true;
                    }
                    else
                    {
                        int disponible = objLog.existencias(int.Parse(dgvDetalle.CurrentRow.Cells[0].Value.ToString()));
                        dgvDetalle.Rows[e.RowIndex].ErrorText = String.Empty;
                        if (int.Parse(e.FormattedValue.ToString()) > disponible)
                            MessageBox.Show(String.Format("EL articulo: {0}, solo cuenta con {1} unidades existencia", dgvDetalle.CurrentRow.Cells[1].Value.ToString(), disponible));
                        else{
                            //limpiar la variable del contador
                            importe = 0;
                            dgvDetalle.CurrentRow.Cells[5].Value = int.Parse(e.FormattedValue.ToString()) * float.Parse(dgvDetalle.CurrentRow.Cells[4].Value.ToString());
                            for (int i = 0; i <= dgvDetalle.RowCount - 1; i++) {
                                //importe += float.Parse(dgvDetalle.CurrentRow.Cells[5].Value.ToString());                            
                                importe += float.Parse(dgvDetalle.Rows[i].Cells[5].Value.ToString());
                                imprimirTotales(importe);
                            }
                        }

                    }
                }
            }
            catch { }
            //}
       
        }

        private void dgvDetalle_CellValidated(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvDetalle_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            dgvDetalle.Rows[e.RowIndex].ErrorText = "El dato introducido no es cantidad";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //objBd.Consulta("INSERT INTO ventas(folio, fecha, id_cliente, enganche, bonificacion_enganche, tot, activo) VALUES('"  "')");
            if (PosSeleccio == -1)
                MessageBox.Show("No has seleccionado el abono");
            else
            {
                try
                {

                    objBd.Consulta(String.Format("INSERT INTO ventas(folio, fecha, id_cliente, enganche, bonificacion_enganche, tot, activo) VALUES({0}, '{1}', '{2}', {3}, {4}, {5}, {6})", folioActual, DateTime.Now.ToString("yyyy/MM/dd"), cmbClientes.SelectedValue, txtEnganche.Text, txtBonifEnganche.Text, txtTotal.Text, 1));
                    int ventaActual;
                    objBd.Consulta("SELECT ISNULL(MAX(id_venta), 0) FROM ventas");
                    if (objBd.Dr.Read())
                    {
                        ventaActual = int.Parse(objBd.Dr.GetValue(0).ToString());
                    }
                    for (int i = 0; i <= dgvDetalle.RowCount; i++)
                    {
                        objBd.Consulta(String.Format("INSERT INTO ventas_detalle(id_venta, id_articulo, cantidad, precio, importe, activo) VALUES({0}, {1}, {2}, {3}, {4}, 1)", folioActual, dgvDetalle.Rows[i].Cells[0].Value.ToString(), dgvDetalle.Rows[i].Cells[3].Value.ToString(), dgvDetalle.Rows[i].Cells[4].Value.ToString(), dgvDetalle.Rows[i].Cells[5].Value.ToString()));

                        //objBd.Consulta(String.Format("Update articulos Set existencia ={0} Where id_articulo ={1} ", "existencias - " int.Parse(dgvDetalle.Rows[i].Cells[3].Value.ToString()) ,dgvDetalle.Rows[i].Cells[0].Value.ToString());
                        objBd.Consulta("Update articulos Set existencia = existencia - " + int.Parse(dgvDetalle.Rows[i].Cells[3].Value.ToString()) + " Where id_articulo = " + dgvDetalle.Rows[i].Cells[0].Value.ToString());
                    }
                    
                }
                catch (Exception ex) { }
                MessageBox.Show("Venta Registrada exitosamente");
                Close();
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            try
            {
                dgvAbonos.Rows.Clear();
                if (int.Parse(cmbClientes.SelectedValue.ToString()) > 0 && int.Parse(cmbArticulos.SelectedValue.ToString()) > 0 && dgvDetalle.RowCount > 0 && double.Parse(txtTotal.Text) > 0)
                {
                    objLog.generarAbonos(3, Double.Parse(txtTotal.Text));
                    //MessageBox.Show(objLog.generarAbonos(3, Double.Parse(txtTotal.Text))[0].ToString());
                    //arreglo de objetos para cachar los valores
                    //double[] 3meses = {objLog.generarAbonos(3, Double.Parse(txtTotal.Text))[0], objLog.generarAbonos(3, Double.Parse(txtTotal.Text))[0], objLog.generarAbonos(3, Double.Parse(txtTotal.Text))[0] };
                    double[] TresMeses = { objLog.generarAbonos(3, Double.Parse(txtTotal.Text))[0], objLog.generarAbonos(3, Double.Parse(txtTotal.Text))[1], objLog.generarAbonos(3, Double.Parse(txtTotal.Text))[2] };
                    double[] SeisMeses = { objLog.generarAbonos(6, Double.Parse(txtTotal.Text))[0], objLog.generarAbonos(6, Double.Parse(txtTotal.Text))[1], objLog.generarAbonos(6, Double.Parse(txtTotal.Text))[2] };
                    double[] NueveMeses = { objLog.generarAbonos(9, Double.Parse(txtTotal.Text))[0], objLog.generarAbonos(9, Double.Parse(txtTotal.Text))[1], objLog.generarAbonos(9, Double.Parse(txtTotal.Text))[2] };
                    double[] DoceMeses = { objLog.generarAbonos(12, Double.Parse(txtTotal.Text))[0], objLog.generarAbonos(12, Double.Parse(txtTotal.Text))[1], objLog.generarAbonos(12, Double.Parse(txtTotal.Text))[2] };
                    //dgvDetalle.Rows.Add(cmbArticulos.SelectedValue, cmbArticulos.Text, modelo, 0, String.Format("{0:00}", objLog.precio(precio).ToString()), 0, "x");
                    dgvAbonos.Rows.Add("3 ABONOS DE", String.Format("${0}", Math.Round(TresMeses[0])), String.Format("TOTAL APAGAR ${0}", Math.Round(TresMeses[1])), String.Format("SE AHORRA {0}", Math.Round(TresMeses[2])), false);
                    dgvAbonos.Rows.Add("6 ABONOS DE", String.Format("${0}", Math.Round(SeisMeses[0])), String.Format("TOTAL APAGAR ${0}", Math.Round(SeisMeses[1])), String.Format("SE AHORRA {0}", Math.Round(SeisMeses[2])), false);
                    dgvAbonos.Rows.Add("9 ABONOS DE", String.Format("${0}", Math.Round(NueveMeses[0])), String.Format("TOTAL APAGAR ${0}", Math.Round(NueveMeses[1])), String.Format("SE AHORRA {0}", Math.Round(NueveMeses[2])), false);
                    dgvAbonos.Rows.Add("12 ABONOS DE", String.Format("${0}", Math.Round(DoceMeses[0])), String.Format("TOTAL APAGAR ${0}", Math.Round(DoceMeses[1])), String.Format("SE AHORRA {0}", Math.Round(DoceMeses[2])), false);
                }
                else
                    MessageBox.Show("Los datos ingresados no son correctos, favor de verificar");
            }catch(Exception ex)
            {
            }
        }


        public void imprimirTotales(float importe)
        {
            //Funcion que imprime en las cjas de texto los totales, que se obtyiene de la clase LogicNegocio en, donde esta la abastraccion del funcionamiento
            double totalEnganche;
            double totalBonificacion;
            double totalGeneral;
            this.txtEnganche.Text = String.Format("{0}", objLog.enganche(importe));
            this.txtBonifEnganche.Text = String.Format("{0}", objLog.bonificacionEnganche(double.Parse(txtEnganche.Text.ToString())));
            this.txtTotal.Text = String.Format("{0}", objLog.totalAdeudo(importe, double.Parse(txtEnganche.Text.ToString()),double.Parse(txtBonifEnganche.Text.ToString())));
        }

        private void dgvAbonos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4) {
                PosSeleccio = -1;               
                //hizo click validar que no haga click en otro
                for (int i = 0; i <= dgvAbonos.RowCount - 1; i++) {
                    if (dgvAbonos.Rows[i].Cells[4].RowIndex != e.RowIndex)
                        dgvAbonos.Rows[i].Cells[4].Value = false;
                    else
                        PosSeleccio = e.RowIndex;
                    //PosSeleccio = e.RowIndex;
                   
                }
            }
        }

        private void dgvAbonos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            
        }


    }
}
