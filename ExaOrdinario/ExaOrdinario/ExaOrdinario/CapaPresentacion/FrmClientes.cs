﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ExaOrdinario.CapaPresentacion
{
    public partial class FrmClientes : ExaOrdinario.CapaPresentacion.FrmBase
    {

        //CapaDatos.BD objBd = new CapaDatos.BD("Data Source=ADRIANA;Initial Catalog=master;Integrated Security=True");
        CapaDatos.BD objBd = new CapaDatos.BD(Program.cadenaConexion);
        CapaNegocios.LogicaNegocio objLogica = new CapaNegocios.LogicaNegocio();
        int modificar = 0;
        int idModificar = 0;
        public Form frmPadre;
        string mensaje;
        public FrmClientes()
        {
            InitializeComponent();
        }

        private void FrmClientes_Load(object sender, EventArgs e)
        {
            //llenarGrid();
        }

        public void llenarGrid()
        {
            objBd.Consulta("Select id_cliente, nombre, apellido_paterno, apellido_materno, rfc From clientes");
            mensaje = "HOLA";
            //DataGridViewRow row = (DataGridViewRow)dgvClientes.Rows
            while (objBd.Dr.Read())
            {
                mensaje += "ID: " + objBd.Dr.GetValue(0) + " Nombre: " + objBd.Dr.GetValue(1) + " Apellido Paterno: " + objBd.Dr.GetValue(2) + " Apellido Materno: " + objBd.Dr.GetValue(3) + " RFC: " + objBd.Dr.GetValue(4) + "\n";
                dgvClientes.Rows.Add(objBd.Dr.GetValue(0), objBd.Dr.GetValue(1), objBd.Dr.GetValue(2), objBd.Dr.GetValue(3), objBd.Dr.GetValue(4));
            }
            FrmMensajes frmMensaje = new FrmMensajes(1, mensaje);
            //frmMensaje.ShowDialog();
            //this.dgvClientes.DataSource = objBd.LlenarGrid("SELECT nombre, apellido_paterno, apellido_materno, rfc FROM clientes");
        }

        public void traerCliente(int idCliente) {
            modificar = 1;
            idModificar = idCliente;
            objBd.Consulta(String.Format("Select  nombre, apellido_paterno, apellido_materno, rfc From clientes Where id_cliente={0}",idCliente));
            if (objBd.Dr.Read())
            {
                txtCliente.Text = objBd.Dr.GetValue(0).ToString();
                this.txtPaterno.Text = objBd.Dr.GetValue(1).ToString();
                this.txtMaterno.Text = objBd.Dr.GetValue(2).ToString();
                this.txtRfc.Text = objBd.Dr.GetValue(3).ToString();

            }
        }

        public void llenarGridClientes(DataGridView tabla) { 
            
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            int continua = 1;
            if (txtCliente.Text == String.Empty)
            {
                MessageBox.Show("No es posible continuar, debe de ingresar Nombre es obligatorio");
                continua = 0;
            }
            if (this.txtPaterno.Text == String.Empty)
            {
                MessageBox.Show("No es posible continuar, debe de ingresar Apellido Paterno es obligatorio");
                continua = 0;
            }
            if (this.txtMaterno.Text == String.Empty)
            {
                MessageBox.Show("No es posible continuar, debe de ingresar Apellido Materno es obligatorio");
                continua = 0;
            }
            if (this.txtRfc.Text == string.Empty)
            {
                MessageBox.Show("No es posible continuar, debe de ingresar RFC es obligatorio");
                continua = 0;
            }
            if (continua == 1)
            {
                if (modificar == 1) 
                    objBd.Consulta("Update clientes SET nombre = '" + txtCliente.Text + "', apellido_paterno='" + txtPaterno.Text + "', apellido_materno = '" + txtMaterno.Text + "',  rfc = '" + txtRfc.Text + "' Where id_cliente = '" + idModificar + "'");
                else
                    objBd.Consulta("INSERT INTO clientes(nombre, apellido_paterno, apellido_materno, rfc, activo) VALUES ('" + txtCliente.Text + "','" + txtPaterno.Text + "','" + txtMaterno.Text + "','" + txtRfc.Text + "',1)");
                MessageBox.Show("Bien Hecho. El cliente ha sido registrado correctamente");           
                Close();
            }
                //this.dgvClientes.DataSource = objBd.LlenarGrid("SELECT nombre, apellido_paterno, apellido_materno, rfc FROM clientes");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {

        }

       /*SELECT        v.id_venta, v.folio, c.id_cliente AS clave_cliente, { fn CONCAT(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) } AS Nombre, v.tot, v.fecha, 'ACTIVA' AS estatus
FROM            ventas AS v INNER JOIN
                         clientes AS c ON v.id_cliente = c.id_cliente
WHERE        (v.activo = 1)*/
    }
}
